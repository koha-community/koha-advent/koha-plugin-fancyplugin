package Koha::FancyWord;

use Modern::Perl;

use base qw(Koha::Object);

=head1 NAME

Koha::FancyWord - Koha Fancy Word Object class

=head1 API

=head2 Class methods

=head3 to_api_mapping

This method returns the mapping for representing a Koha::Patron object
on the API.

=cut

sub to_api_mapping {
    return {
        id => 'fancy_word_id'
    };
}

=head2 Internal methods

=head3 _type

=cut

sub _type {
        return 'KohaPluginFancypluginWordsList';
}

1;
