use utf8;
package Koha::Schema::Result::KohaPluginFancypluginWordsList;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Koha::Schema::Result::KohaPluginFancypluginWordsList

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<koha_plugin_fancyplugin_words_list>

=cut

__PACKAGE__->table("koha_plugin_fancyplugin_words_list");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 fancy_word

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 timestamp

  data_type: 'timestamp'
  datetime_undef_if_invalid: 1
  default_value: current_timestamp
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "fancy_word",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "timestamp",
  {
    data_type => "timestamp",
    datetime_undef_if_invalid => 1,
    default_value => \"current_timestamp",
    is_nullable => 0,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2020-12-04 15:38:37
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:MBv0PVHfqAYQ72A5ju0gww

sub koha_object_class {
    'Koha::FancyWord';
}

sub koha_objects_class {
    'Koha::FancyWords';
}

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
