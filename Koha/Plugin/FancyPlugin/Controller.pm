package Koha::Plugin::FancyPlugin::Controller;

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# This program comes with ABSOLUTELY NO WARRANTY;

use Modern::Perl;

use Mojo::Base 'Mojolicious::Controller';

use Koha::FancyWords;

use Try::Tiny;

=head1 Koha::Plugin::FancyPlugin::Controller

A class implementing the controller code handling fancy words

=head2 Class methods

=head3 list

Controller function that handles listing Koha::FancyWord objects

=cut

sub list {
    my $c = shift->openapi->valid_input or return;

    return try {
        my $fancy_words_rs = Koha::FancyWords->search;
        my $fancy_words    = $c->objects->search( $fancy_words_rs );

        return $c->render(
            status  => 200, # https://wiki.koha-community.org/wiki/Coding_Guidelines_-_API#SWAGGER3.2.2_GET
            openapi => $fancy_words
        );
    }
    catch {
        $c->unhandled_exception($_);
    };
}


=head3 get

Controller function that handles retrieving a single Koha::FancyWord object

=cut

sub get {
    my $c = shift->openapi->valid_input or return;

    return try {
        my $fancy_word_id = $c->validation->param('fancy_word_id');
        my $fancy_word    = Koha::FancyWords->find($fancy_word_id);

        unless ($fancy_word) {
            return $c->render(
                status  => 404,
                openapi => { error => "Fancy word not found." }
            );
        }

        return $c->render(
            status  => 200, # https://wiki.koha-community.org/wiki/Coding_Guidelines_-_API#SWAGGER3.2.2_GET
            openapi => $fancy_word->to_api
        );
    }
    catch {
        $c->unhandled_exception($_);
    };
}

=head3 add

Controller function that handles adding a new Koha::FancyWord object

=cut

sub add {
    my $c = shift->openapi->valid_input or return;

    return try {
        # Use ->new_from_api, which does any attribute mapping that is required
        my $fancy_word = Koha::FancyWord->new_from_api( $c->validation->param('body') )->store;
        $fancy_word->discard_changes;
        # https://wiki.koha-community.org/wiki/Coding_Guidelines_-_API#SWAGGER3.4.1_POST
        $c->res->headers->location( $c->req->url->to_string . '/' . $fancy_word->id );

        return $c->render(
            status  => 201, # https://wiki.koha-community.org/wiki/Coding_Guidelines_-_API#SWAGGER3.2.1_POST
            openapi => $fancy_word->to_api
        );
    }
    catch {
        $c->unhandled_exception($_);
    };
}


=head3 update

Controller function that handles updating a Koha::FancyWord object

=cut

sub update {
    my $c = shift->openapi->valid_input or return;

    my $fancy_word_id = $c->validation->param('fancy_word_id');
    my $fancy_word    = Koha::FancyWords->find( $fancy_word_id );

    unless ($fancy_word) {
         return $c->render(
             status  => 404,
             openapi => { error => "Fancy word not found" }
         );
     }

    return try {
        my $body = $c->validation->param('body');
        my $user = $c->stash('koha.user');

        $fancy_word->set_from_api($c->validation->param('body'))->store;
        # We need to return the updated object
        $fancy_word->discard_changes; # https://wiki.koha-community.org/wiki/Coding_Guidelines_-_API#SWAGGER3.3.3_PUT

        return $c->render(
            status  => 200, # https://wiki.koha-community.org/wiki/Coding_Guidelines_-_API#SWAGGER3.2.3_PUT
            openapi => $fancy_word->to_api
        );
    }
    catch {
        $c->unhandled_exception($_);
    };
}

=head3 delete

Controller function that handles deleting a Koha::FancyWord object

=cut

sub delete {
    my $c = shift->openapi->valid_input or return;

    my $fancy_word = Koha::FancyWords->find( $c->validation->param('fancy_word_id') );

    unless ( $fancy_word ) {
        return $c->render(
            status  => 404,
            openapi => { error => "Fancy word not found" }
        );
    }

    return try {

        $fancy_word->delete;

        return $c->render(
            status  => 204, # https://wiki.koha-community.org/wiki/Coding_Guidelines_-_API#SWAGGER3.2.4_DELETE
            openapi => q{}
        );
    } catch {
        $c->unhandled_exception($_);
    };
}

1;
